﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgrammationObjet.ListeChainee
{
    public class Element
    {
        private object _Objet;
        private Element _Suivant;

        public object Objet
        {
            get { return this._Objet ; }
            set { this._Objet = value ;  }
        }

        public Element Suivant
        {
            get { return this._Suivant ; }
            set { this._Suivant = value ; }
        }

        public Element(object o) {
            {
                this._Objet = o;
                this._Suivant = null;
            } }
    }

    public class Liste
    {
        private Element _Debut;
        private int _NbElements;

        public int NbElements
        {
            get { return this._NbElements; }
        }

        public Element this[int i]
        {
            get
            {
                Element e = this._Debut;
                for (int j = 0; j < i; j++)
                {
                    e = e.Suivant;
                }
                return e;
            }
        }
          
        public void InsererDebut(object o)
        {
            Element e = this._Debut;
            this._Debut = new Element(o);
            this._Debut.Suivant = e;
            this._NbElements++;
        }

        public void InsererFin(object o)
        {
            Element e = this._Debut;
            if(e != null)
            {
                while (e.Suivant != null)
                {
                    e = e.Suivant;
                }
                e.Suivant = new Element(o);
            } else
            {
                this._Debut = new Element(o);
            }
            this._NbElements++;
        }

        public void Lister()
        {
            Element e = this._Debut;
            while (e.Suivant != null)
            {
                Console.WriteLine(e.Objet.ToString());
                e = e.Suivant;
            }
            Console.WriteLine(e.Objet.ToString());
        }

        public void Vider()
        {
            Element e = this._Debut;
            if (e != null)
            {
                e.Objet = null;
                this._Debut = e.Suivant;
                e.Suivant = null;
                this.Vider();
            }
            this._NbElements = 0;
        }

        public Liste()
        {
            this._Debut = null;
            this._NbElements = 0;
        }
    }
}
