﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ProgrammationObjet.Societe
{
    public class Personne
    {
        private string _Nom;
        private string _Prenom;
        private int _Age;

        public string Nom
        {
            get { return this._Nom; }
            set { this._Nom = value; }
        }

        public string Prenom
        {
            get { return this._Prenom; }
            set { this._Prenom = value; }
        }

        public int Age
        {
            get { return this._Age; }
            set { this._Age = value; }
        }

        public override string ToString()
        {
            return "Personne : [ "+this._Nom+" ; "+this._Prenom+" ; "+this._Age+" ]";
        }

        public virtual void Afficher()
        {
            Console.WriteLine(this.ToString());
        }

        public static Personne operator ++(Personne p) => new Personne(p._Nom, p._Prenom, p._Age+1);

        public Personne(string nom, string prenom)
        {
            this._Nom = nom;
            this._Prenom = prenom;
            this._Age = 0;
        }

        public Personne(string nom, string prenom, int age)
        {
            this._Nom = nom;
            this._Prenom = prenom;
            this._Age = age;
        }
    }
}
