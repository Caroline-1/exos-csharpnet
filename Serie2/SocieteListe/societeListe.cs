using System;
using System.Collections.Generic;
using System.Text;
using ProgrammationObjet.Societe;
using ProgrammationObjet.ListeChainee;

namespace ProgrammationObjet.SocieteListe
{
    public class GererSocieteListe
    {
        static void Main(string[] args)
        {
            Liste liste = new Liste();
            liste.InsererDebut(new Employe("Rave", "Beth", 32, 1650.93));
            liste.InsererDebut(new Employe("Hyère", "Claire", 27, 1981.12));
            liste.InsererDebut(new Employe("Tine", "Clément", 46, 1650.93));
            liste.InsererDebut(new Employe("Halatette", "Djamal", 26, 1650.93));
            liste.InsererDebut(new Employe("Vierre", "Marie", 39, 1981.12));
            liste.InsererDebut(new Chef("Orial", "Edith", 51, 2689.36, "Commercial"));
            liste.InsererDebut(new Chef("Prauchain", "Edmond", 47, 2151.49, "Support Technique"));
            liste.InsererDebut(new Directeur("Emoix", "Elise", 33, 3478.56, "Ressources Humaines", "Hiver, Automne et Printemps"));

            Console.WriteLine("Affichage 1 ----------------");
            liste.Lister();
            liste.Vider();

            liste.InsererFin(new Employe("Rave", "Beth", 32, 1650.93));
            liste.InsererFin(new Employe("Hyère", "Claire", 27, 1981.12));
            liste.InsererFin(new Employe("Tine", "Clément", 46, 1650.93));
            liste.InsererFin(new Employe("Halatette", "Djamal", 26, 1650.93));
            liste.InsererFin(new Employe("Vierre", "Marie", 39, 1981.12));
            liste.InsererFin(new Chef("Orial", "Edith", 51, 2689.36, "Commercial"));
            liste.InsererFin(new Chef("Prauchain", "Edmond", 47, 2151.49, "Support Technique"));
            liste.InsererFin(new Directeur("Emoix", "Elise", 33, 3478.56, "Ressources Humaines", "Hiver, Automne et Printemps"));
            Console.WriteLine("Affichage 2 ----------------");
            liste.Lister();

            Console.WriteLine("Affichage 3 ----------------");
            for (int i = 0; i < liste.NbElements; i++)
            {
                Console.WriteLine(liste[i].Objet.ToString());
            }
         }
    }
}
