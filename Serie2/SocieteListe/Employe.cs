﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Reflection.Metadata;
using System.Text;

namespace ProgrammationObjet.Societe
{
    public class Employe : Personne
    {
        private double _Salaire;
        public double Salaire
        {
            get { return this._Salaire; }
            set { this._Salaire = value; }
        }

        public override string ToString()
        {
            return "Employe : [ " + this.Nom + " ; " + this.Prenom + " ; " + this.Age + " ; " + this._Salaire + " ]";
        }

        public override void Afficher()
        {
            Console.WriteLine(this.ToString());
        }

       
        public Employe(string n, string p, int a, double s) : base(n, p, a)
        {
            this._Salaire = s;
        }
    }
}
