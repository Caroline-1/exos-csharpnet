using System;
using System.Collections.Generic;
using System.Text;

namespace ProgrammationObjet.Personnes
{
    class TestPersonne
    {
        static void Main(string[] args)
        {
            Personne p1 = new Personne("Croche", "Sarah", 25);
            Personne p2 = new Personne("Terrieur", "Alain", 48);
            Personne p3 = new Personne("Terrieur", "Alex", 48);

            Console.WriteLine(Personne.Combien());

            p1.Afficher();
            p2.Afficher();
            p3.Afficher();
        }
    }
}
