﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProgrammationObjet.Personnes
{
    public class Personne
    {
        private string nom;
        private string prenom;
        private int age;
        static int NbPersonnes;
        static Personne()
        {
            NbPersonnes = 0;
        }

        public void Afficher()
        {
            Console.WriteLine("Nom : "+this.nom+"; Prenom : "+this.prenom+"; Age : "+this.age);
        }

        static public int Combien()
        {
            return NbPersonnes;
        }

        public Personne(string n, string p, int a)
        {
            this.nom = n;
            this.prenom = p;
            this.age = a;
            NbPersonnes++;
        }
    }
}
