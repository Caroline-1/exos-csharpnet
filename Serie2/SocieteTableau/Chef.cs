﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgrammationObjet.SocieteTableau
{
    class Chef : Employe
    {
        private string _Service;
        public string Service
        {
            get { return this._Service; }
            set { this._Service = value; }
        }

        public override string ToString()
        {
            return "Chef : [ " + this.Nom + " ; " + this.Prenom + " ; " + this.Age + " ; " + this.Salaire + " ; " + this._Service + " ]";
        }

        public override void Afficher()
        {
            Console.WriteLine(this.ToString());
        }

        
        public Chef(string n, string p, int a, double sa, string se) : base(n, p, a, sa)
        {
            this._Service = se;
        }
    }
}
