﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgrammationObjet.SocieteTableau
{
    class Directeur : Chef
    {
        private string _Societe;
        public string Societe
        {
            get { return this._Societe; }
            set { this._Societe = value; }
        }

        public override string ToString()
        {
            return "Directeur : [ " + this.Nom + " ; " + this.Prenom + " ; " + this.Age + " ; " + this.Salaire + " ; " + this.Service + " ; " + this._Societe + " ]";
        }

        public override void Afficher()
        {
            Console.WriteLine(this.ToString());
        }

        
        public Directeur(string n, string p, int a, double sa, string se, string so) : base(n, p, a, sa, se)
        {
            this._Societe = so;
        }
    }
}
