﻿using System;
using System.Collections.Generic;
using System.Text;
using ProgrammationObjet.SocieteTableau;

namespace ProgrammationObjet.SocieteTableau
{
    public class societeTableau
    {
        static void Main(string[] args)
        {
            Personne[] tab = new Personne[] { new Employe("Rave", "Beth", 32, 1650.93),
                                              new Employe("Hyère", "Claire", 27, 1981.12),
                                              new Employe("Tine", "Clément", 46, 1650.93),
                                              new Employe("Halatette", "Djamal", 26, 1650.93),
                                              new Employe("Vierre", "Marie", 39, 1981.12),
                                              new Chef("Orial", "Edith", 51, 2689.36, "Commercial"),
                                              new Chef("Prauchain", "Edmond", 47, 2151.49, "Support Technique"),
                                              new Directeur("Emoix", "Elise", 33, 3478.56, "Ressources Humaines", "Hiver, Automne et Printemps")};

            Console.WriteLine("tab : ");            
            for (int i = 0; i < 8; i++)
            {
                Console.Write(i + 1 + " --> ");
                tab[i].Afficher();
            }

            ++tab[0];

            int j = 1;
            foreach (Personne p in tab)
            {
                Console.Write(j + " --> ");
                p.Afficher();
                j++;
            }
            
        }
    }
}
