﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ProgrammationObjet.ListeChainee
{
    public class ListeEnumeration : IEnumerator
    {
        private Liste _Liste;
        private int _indiceCourant;

        public Element Current
        {
            get { return this._Liste[this._indiceCourant]; }
        }

        object IEnumerator.Current => throw new NotImplementedException();

        public void MoveNext()
        {
            this._indiceCourant++;
        }

        public void Reset()
        {
            this._indiceCourant = 0;
        }

        bool IEnumerator.MoveNext()
        {
            throw new NotImplementedException();
        }

        public ListeEnumeration(Liste l)
        {
            this._Liste = l;
            this._indiceCourant = 0;
        }

    }
}
