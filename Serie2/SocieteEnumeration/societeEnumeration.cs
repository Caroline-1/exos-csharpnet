using ProgrammationObjet.ListeChainee;
using ProgrammationObjet.Societe;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgrammationObjet.SocieteEnumeration
{
    public class GererSocieteEnumeration
    {
        static void Main(string[] args)
        {
            Liste liste = new Liste();
            liste.InsererDebut(new Employe("Rave", "Beth", 32, 1650.93));
            liste.InsererDebut(new Employe("Hyère", "Claire", 27, 1981.12));
            liste.InsererDebut(new Employe("Tine", "Clément", 46, 1650.93));
            liste.InsererDebut(new Employe("Halatette", "Djamal", 26, 1650.93));
            liste.InsererDebut(new Employe("Vierre", "Marie", 39, 1981.12));
            liste.InsererDebut(new Chef("Orial", "Edith", 51, 2689.36, "Commercial"));
            liste.InsererDebut(new Chef("Prauchain", "Edmond", 47, 2151.49, "Support Technique"));
            liste.InsererDebut(new Directeur("Emoix", "Elise", 33, 3478.56, "Ressources Humaines", "Hiver, Automne et Printemps"));

            Console.WriteLine("Affichage 1 ----------------");
            for (int i = 0; i < liste.NbElements; i++)
            {
                Console.WriteLine(liste[i].Objet.ToString());
            }


            ListeEnumeration e = new ListeEnumeration(liste);

            Console.WriteLine("Affichage 2 ----------------");
            while(e.Current != null)
            {
                Console.WriteLine(e.Current.Objet.ToString());
                e.MoveNext();
            }
        }
    }
}
