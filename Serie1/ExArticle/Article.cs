﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LesBases.ExArticle
{
    public class Article
    {
        public string Nom;
        protected double Prix;
        protected int Quantite;

        public void Afficher()
        {
            Console.WriteLine("Nom : " + this.Nom + "; Prix : " + this.Prix + "; Quantite : " + this.Quantite);
        }

        public void Ajouter(int value)
        {
            this.Quantite = this.Quantite + value;
        }

        public void Retirer(int value)
        {
            this.Quantite = this.Quantite - value;
        }

        public Article(string nom, double prix, int quantite)
        {
            this.Nom = nom;
            this.Prix = prix;
            this.Quantite = quantite;
        }


    }
}
