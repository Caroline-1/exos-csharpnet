﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LesBases.ExArticleType
{
    public enum TypeOfArticle { droguerie, habillement, loisir, bricolage, divers, undef};
    public class Article
    {
        public string Nom;
        protected double Prix;
        protected int Quantite;
        protected TypeOfArticle Type;

        public void Afficher()
        {
            Console.WriteLine("Nom : " + this.Nom + "; Prix : " + this.Prix + "; Quantite : " + this.Quantite + "; Type : " + this.Type);
        }

        public void Ajouter(int value)
        {
            this.Quantite = this.Quantite + value;
        }

        public void Retirer(int value)
        {
            this.Quantite = this.Quantite - value;
        }

        public void setType(TypeOfArticle t)
        {
            this.Type = t;
        }

        public Article(string nom, double prix, int quantite, TypeOfArticle t)
        {
            this.Nom = nom;
            this.Prix = prix;
            this.Quantite = quantite;
            this.Type = t;
        }


    }
}
