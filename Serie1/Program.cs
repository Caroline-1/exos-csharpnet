﻿using System;
using LesBases.Classes;
using LesBases.ExArticle;
using LesBases.ExArticleType;
namespace LesBases
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Classes.Article a = new Classes.Article();
            a.setDesignation("video");
            Console.WriteLine(a.getDesignation());

            ExArticle.Article a1 = new ExArticle.Article("Ampoule", 3.95, 20);
            ExArticle.Article a2 = new ExArticle.Article("Marche-pieds", 39.99, 35);
            a1.Afficher();
            a2.Afficher();
            a1.Ajouter(10);
            a2.Retirer(5);
            a1.Afficher();
            a2.Afficher();
            Console.WriteLine("Entrer le nom de l'article : ");
            string nom = Console.ReadLine();
            Console.WriteLine("Entrer le prix de l'article : ");
            double prix = System.Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Entrer la quantite de l'article : ");
            int quantite = System.Convert.ToInt32(Console.ReadLine());
            ExArticle.Article a3 = new ExArticle.Article(nom, prix, quantite);
            a3.Afficher();

            ExArticleType.Article A1 = new ExArticleType.Article("Livre", 15.99, 50, TypeOfArticle.undef);
            ExArticleType.Article A2 = new ExArticleType.Article("Souris", 18.99, 250, TypeOfArticle.divers);
            A1.Afficher();
            A2.Afficher();
            A1.Ajouter(10);
            A2.Retirer(10);
            A1.Afficher();
            A2.Afficher();
            Console.WriteLine("Entrer le type de l'article : ");
            TypeOfArticle t = (TypeOfArticle) Enum.Parse(typeof(TypeOfArticle), Console.ReadLine(), true);
            A1.setType(t);
            A1.Afficher();

            ExArticleType.Article A3 = new ExArticleType.Article("Bouteilles d'eau 1Lx6", 2.99, 1000, TypeOfArticle.droguerie);
            ExArticleType.Article[] tab = new ExArticleType.Article[3];
            tab[0] = A1;
            tab[1] = A2;
            tab[2] = A3;
            Console.WriteLine("tab : ");
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(i + 1);
                tab[i].Afficher();
            }
        }
    }
}
