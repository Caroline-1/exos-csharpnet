﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LesBases.Classes
{
    public class Article
    {
        protected string designation;
        protected double prix;

        public void setDesignation(string value)
        {
            this.designation = value;
        }

        public void setPrix(double value)
        {
            this.prix = value;
        }

        public string getDesignation()
        {
            return this.designation;
        }

        public double getPrix()
        {
            return this.prix;
        }
        public string acheter()
        {
            return this.designation + " " + this.prix;
        }
    }
}
