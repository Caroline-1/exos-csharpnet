﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LesBases.Classes
{
    class Livre : Article
    {
        protected string isbn { get; set; }
        protected int nbPages { get; set; }
    }
}
